export default function routing($stateProvider) {
    'ngInject';
    $stateProvider
      .state('resourceExample', {
        url: '/resourceExample',
        component: 'resourceExample'
      });
}
