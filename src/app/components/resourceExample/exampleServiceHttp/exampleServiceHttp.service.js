export class ExampleServiceHttp {
  constructor($http) {
    'ngInject;'
    this.$http = $http;
  }
  getItems() {
    return this.$http.get('https://private-4b671-exampleservice.apiary-mock.com/api/pets').then(response => response.data);
  }
}
