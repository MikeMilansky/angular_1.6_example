export const ResourceExampleComponent = {
  template: require('./resourceExample.template.html'),
  controller: class ResourceExampleComponent {
    constructor(ExampleServiceHttp, ExampleServiceResource) {
      'ngInject';
      this.serviceHttp = ExampleServiceHttp;
      this.serviceResource = ExampleServiceResource;
    }
    $onInit() {
      this.items = [];
      //requestParams
      this.getParams = '{ "id": 3, "queryParam": "im_in_query" }';
      this.saveParams = '{"name":"fugiat"}';
      this.deleteParams = '{ "id": 5 }';

      //responses
      this.queryResponse = '';
      this.getResponse = '';
      this.saveResponse = '';
      this.deleteResponse = '';


      this.isLoading = false;
      this.serviceHttp.getItems().then(response => this.items = response); // get items via $http
      this.items = this.serviceResource.query(); // get items via $resource
    }
    //A lot of copy and paste for example
    loadCollection() {
      // https://docs.angularjs.org/api/ngResource/service/$resource
      this.isLoading = true;
      this.items = this.serviceResource.query(() => {
        this.isLoading = false;
        this.queryResponse = angular.toJson(this.items);
      });
    }
    getItem() {
      this.isLoading = true;
      this.serviceResource.get(angular.fromJson(this.getParams), (res) => {
        this.isLoading = false;
        this.getResponse = angular.toJson(res);
      });
    }
    saveItem() {
      this.isLoading = true;
      this.serviceResource.save(angular.fromJson(this.saveParams), (res) => {
        this.isLoading = false;
        this.saveResponse = angular.toJson(res);
      });
    }
    deleteItem() {
      this.isLoading = true;
      this.serviceResource.delete(angular.fromJson(this.deleteParams), (res) => {
        this.isLoading = false;
        this.deleteResponse = angular.toJson(res);
      });
    }
  }
};
