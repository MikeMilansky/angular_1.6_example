import angular from 'angular';
import uiRouter from 'angular-ui-router';
import { ResourceExampleComponent } from './resourceExample.component';
import routing from './resourceExample.routes';
import './resourceExample.scss';

export const ResourceExampleModule = angular
  .module('resourceExample', [
    uiRouter
  ])
  .component('resourceExample', ResourceExampleComponent)
  .config(routing)
  .name;
