import angular from 'angular';
import uirouter from 'angular-ui-router';
import localStorage from 'angular-local-storage';
import uiBootstrap from 'angular-ui-bootstrap';
import './../../node_modules/bootstrap/dist/css/bootstrap.min.css';

import routing from './routing';
import config from './config';
import run from './run';

///CUSTOM MODULES

import { ResourceExampleModule } from './components/resourceExample/resourceExample.module';
import { AppComponent } from './components/app/app.component';
import { ServicesModule } from './services/services.module.js';

angular
	.module('app', [
		uirouter,
		ResourceExampleModule,
		ServicesModule
	])
	.component('app', AppComponent)
	.config(routing)
	.config(config)
	.run(run);
