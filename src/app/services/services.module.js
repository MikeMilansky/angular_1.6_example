import angular from 'angular';
import { ExampleServiceHttp } from './exampleServiceHttp/exampleServiceHttp.service';
import { ExampleServiceResource } from './exampleServiceResource/ExampleServiceResource.service';
import angularResource from 'angular-resource';

export const ServicesModule = angular
  .module('app.services', [angularResource])
  .service('ExampleServiceHttp', ExampleServiceHttp)
  .service('ExampleServiceResource', ExampleServiceResource)
  .name;
