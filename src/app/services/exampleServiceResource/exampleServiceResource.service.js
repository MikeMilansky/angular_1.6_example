export class ExampleServiceResource {
  constructor($resource) {
    'ngInject;'
    return $resource('https://private-4b671-exampleservice.apiary-mock.com/api/pets/:id', { id: '@id' });
  }
}
