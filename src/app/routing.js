export default function routing($urlRouterProvider) {
  $urlRouterProvider.otherwise('/resourceExample');
}
